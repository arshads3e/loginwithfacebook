<?php
session_start();
require 'Facebook_PHP_SDK/autoload.php';
$fb = new Facebook\Facebook([
  'app_id' => '', // Replace {app-id} with your app id
  'app_secret' => '',
  'default_graph_version' => '',
  ]);

$helper = $fb->getRedirectLoginHelper();

$permissions = ['email']; // Optional permissions
$loginUrl = $helper->getLoginUrl('https://demo.com/login_redirect.php', $permissions);

// echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';

header("location:".$loginUrl);
?>